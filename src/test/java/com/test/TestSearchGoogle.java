package com.test;

import org.testng.annotations.Test;

import com.base.Base;
import com.page.GoogleSearchPage;

public class TestSearchGoogle extends Base {

	@Test
	public void testSearchGoogle() throws InterruptedException {
		GoogleSearchPage searchPage = new GoogleSearchPage();
		super.configureBrowser();

		log.info("1 : Open Google search page");
		super.openBrowser("https://www.google.com");

		log.info("2 : search Keyword ");
		searchPage.searchKeyword("Selenium webdriver");

		log.info("3 : waiting 10 seconds before closing the browser");
		Thread.sleep(10 * 1000);

		log.info("4 : Closing browser");
		super.closeBrowser();

	}

}
