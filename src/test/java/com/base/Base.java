package com.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Base {

	public static WebDriver driver;

	public static WebDriverWait wait;

	public static final Logger log = LoggerFactory.getLogger(Base.class.getName());

	public void configureBrowser() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 20 * 1000);

	}

	public void openBrowser(String Url) {
		log.info("Opening the browser using the url : " + Url);
		driver.get(Url);
	}

	public void closeBrowser() {
		log.info("Close the browser ");
		driver.close();
	}

	public void click(By xpath) {
		log.info("Click on element : " + xpath);
		driver.findElement(xpath).click();

	}

	public void setText(By xpath, String value) {

		log.info("Setting text : " + value);
		driver.findElement(xpath).click();
		driver.findElement(xpath).clear();
		driver.findElement(xpath).sendKeys(value);

	}

	public String getText(By xpath) {
		log.info("getteing element text !");
		return driver.findElement(xpath).getText();

	}

	public void waitForElementToBeVisible(By xpath) {
		log.info("Waiting for element visibility : " + xpath);
		wait.until(ExpectedConditions.visibilityOfElementLocated(xpath));

	}

	public void waitForElementToBePresent(By xpath) {
		log.info("Waiting for element presence : " + xpath);
		wait.until(ExpectedConditions.presenceOfElementLocated(xpath));

	}

	public static void waitForElementToBeClickable(By xpath) {
		log.info("Waiting for element to be clickable : " + xpath);
		wait.until(ExpectedConditions.elementToBeClickable(xpath));
	}

}
