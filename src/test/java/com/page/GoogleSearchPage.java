package com.page;

import org.openqa.selenium.By;

import com.base.Base;

public class GoogleSearchPage extends Base{
	
	private By inputSearchXpath = By.xpath("//input[@name='q']");
	private By buttonSearchXpath = By.xpath("(//input[@aria-label='Recherche Google'])[2]");
	
	
	public void searchKeyword(String Keyword) {
		super.waitForElementToBePresent(inputSearchXpath);
		super.waitForElementToBeVisible(inputSearchXpath);
		
		super.setText(inputSearchXpath, Keyword);
		
		super.click(buttonSearchXpath);
		
	}
	
	

}
